﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TerrainGenerator : MonoBehaviour
{
    const float viewerMoveThresholdForChunkUpdate = 25f;
    const float sqrViewerMoveThresholdForChunkUpdate = viewerMoveThresholdForChunkUpdate * viewerMoveThresholdForChunkUpdate;

    public int colliderLODIndex;
    public LODInfo[] detailLevels;

    public MeshSettings meshSettings;
    public NavMeshSurface surface;

    [SerializeField]
    private int levelWidthInTiles, levelDepthInTiles;

    [SerializeField]
    private TreeGeneration treeGeneration;

    [SerializeField]
    private RiverGeneration riverGeneration;

    private TerrainData terrainData;
    public HeightMapSettings heightMapSettings;
    public static HeightMapSettings Static_HeightMapSettings;
    public TextureData textureSettings;

    public Transform viewer;
    public Material mapMaterial;
    public TileCoordinate chunkCoord;

    [HideInInspector]
    public int tileDepthInVertices;
    [HideInInspector]
    public int tileWidthInVertices;

    Vector2 viewerPosition;
    Vector2 viewerPositionOld;

    float meshWorldSize;
    int chunksVisibleInViewDst;

    bool river = false;

    Dictionary<Vector2, TerrainChunk> terrainChunkDictionary = new Dictionary<Vector2, TerrainChunk>();
    List<TerrainChunk> visibleTerrainChunks = new List<TerrainChunk>();



    private void Awake()
    {
        Static_HeightMapSettings = heightMapSettings;

    }

    void Start()
    {

        textureSettings.ApplyToMaterial(mapMaterial);
        textureSettings.UpdateMeshHeights(mapMaterial, heightMapSettings.minHeight, heightMapSettings.maxHeight);

        float maxViewDst = detailLevels[detailLevels.Length - 1].visibleDstThreshold;
        meshWorldSize = meshSettings.meshWorldSize;
        chunksVisibleInViewDst = Mathf.RoundToInt(maxViewDst / meshWorldSize);

        

        // calculate the number of vertices of the tile in each axis using its mesh
        //Vector3[] tileMeshVertices = tileSize;
        tileDepthInVertices = meshSettings.numVertsPerLine;
        tileWidthInVertices = tileDepthInVertices;

        // build an empty LevelData object, to be filled with the tiles to be generated
        terrainData = new TerrainData(tileDepthInVertices, tileWidthInVertices, this.levelDepthInTiles, this.levelWidthInTiles);
        UpdateVisibleChunks();
    }

    void Update()
    {
        viewerPosition = new Vector2(viewer.position.x, viewer.position.z);

        if (viewerPosition != viewerPositionOld)
        {
            foreach (TerrainChunk chunk in visibleTerrainChunks)
            {
                chunk.UpdateCollisionMesh();
            }

        }

        if ((viewerPositionOld - viewerPosition).sqrMagnitude > sqrViewerMoveThresholdForChunkUpdate)
        {
            viewerPositionOld = viewerPosition;
            UpdateVisibleChunks();
        }

        if (terrainData.chunkCount == 9)
        {
            //After chunks are created and loaded, build the river, then send resulting heightmaps to OnHeightMapReceived function inside each TerrainChunk
            //this is where the chunks are all finally loaded so we will now populate the biome mesh with rivers, resources, and starting positions
            riverGeneration.GenerateRivers(this.levelWidthInTiles * tileWidthInVertices, this.levelDepthInTiles * tileDepthInVertices, this.terrainData);
            river = true;
            UpdateVisibleChunks();
            terrainData.chunkCount = 0;
            treeGeneration.GenerateTrees(this.levelWidthInTiles,  this.tileWidthInVertices, this.terrainData);
        }

    }

    void UpdateVisibleChunks()
    {
        HashSet<Vector2> alreadyUpdatedChunkCoords = new HashSet<Vector2>();
        for (int i = visibleTerrainChunks.Count - 1; i >= 0; i--)
        {
            alreadyUpdatedChunkCoords.Add(visibleTerrainChunks[i].coord);
        }

        int currentChunkCoordX = Mathf.RoundToInt(viewerPosition.x / meshWorldSize);
        int currentChunkCoordY = Mathf.RoundToInt(viewerPosition.y / meshWorldSize);

        for (int yOffset = -chunksVisibleInViewDst; yOffset <= chunksVisibleInViewDst; yOffset++)
        {
            for (int xOffset = -chunksVisibleInViewDst; xOffset <= chunksVisibleInViewDst; xOffset++)
            {
                Vector2 viewedChunkCoord = new Vector2(currentChunkCoordX + xOffset, currentChunkCoordY + yOffset);
                if (!alreadyUpdatedChunkCoords.Contains(viewedChunkCoord))
                {  

                    if (terrainChunkDictionary.ContainsKey(viewedChunkCoord))
                    {
                        terrainChunkDictionary[viewedChunkCoord].OnHeightMapReceived(terrainData.chunksData[(int)xOffset + (int)(levelDepthInTiles/2), (int)yOffset + (int)(levelDepthInTiles / 2)]);
                        terrainChunkDictionary[viewedChunkCoord].UpdateTerrainChunk();
                        terrainChunkDictionary[viewedChunkCoord].UpdateCollisionMesh();
                        surface.BuildNavMesh();
                    }

                    else
                    {
                        TerrainChunk newChunk = new TerrainChunk(viewedChunkCoord, heightMapSettings, meshSettings, detailLevels, colliderLODIndex, transform, viewer, mapMaterial, terrainData);

                        terrainChunkDictionary.Add(viewedChunkCoord, newChunk);
                        newChunk.onVisibilityChanged += OnTerrainChunkVisibilityChanged;
                        //this line is not in the original download, unknown origin
                        //newChunk.OnHeightMapCompleted = (map) => { textureSettings.ApplyToMaterial(mapMaterial, map); };
                        newChunk.Load();
                    }
                }
             }
        }

    }

    void OnTerrainChunkVisibilityChanged(TerrainChunk chunk, bool isVisible)
    {
        if (isVisible)
        {
            visibleTerrainChunks.Add(chunk);
        }
        else
        {
            visibleTerrainChunks.Remove(chunk);
        }
    }

}


public class TerrainData
{
    private int tileDepthInVertices, tileWidthInVertices;
    public int levelDepthInTiles;

    public int chunkCount = 0;

    public HeightMap[,] chunksData;

    public TerrainData(int tileDepthInVertices, int tileWidthInVertices, int levelDepthInTiles, int levelWidthInTiles)
    {
        // build the tilesData matrix based on the level depth and width
        chunksData = new HeightMap[levelWidthInTiles, levelDepthInTiles];

        this.tileDepthInVertices = tileDepthInVertices;
        this.tileWidthInVertices = tileWidthInVertices;
        this.levelDepthInTiles = levelDepthInTiles;
    }

    public void AddMapData(HeightMap mapData, int tileXIndex, int tileZIndex)
    {
        //// save the TileData in the corresponding coordinate
        chunksData[tileXIndex+(int)(levelDepthInTiles/2), tileZIndex+ (int)(levelDepthInTiles / 2)] = mapData;

        //Debug.Log("Maxheat: " + mapData.maxHeat);
        
        //Debug.Log("X " + tileXIndex + " int stuff: " + (int)(levelDepthInTiles / 2) + " z: " + tileZIndex);
//        if ((tileXIndex + (int)(levelDepthInTiles / 2) ) == 0 && (tileZIndex + (int)(levelDepthInTiles / 2)) == 0)
//        {
//
//       }
        chunkCount++;
    }

    public TileCoordinate ConvertToTileCoordinate(int xIndex, int zIndex)
    {
        // the tile index is calculated by dividing the index by the number of tiles in that axis
        int tileXIndex = (int)Mathf.Floor((float)xIndex / (float)this.tileWidthInVertices);
        int tileZIndex = (int)Mathf.Floor((float)zIndex / (float)this.tileDepthInVertices);
        // the coordinate index is calculated by getting the remainder of the division above
        // we also need to translate the origin to the bottom left corner
        int coordinateXIndex = this.tileWidthInVertices - (xIndex % this.tileDepthInVertices) - 1;
        int coordinateZIndex = this.tileDepthInVertices - (zIndex % this.tileDepthInVertices) - 1;

        TileCoordinate tileCoordinate = new TileCoordinate(tileXIndex, tileZIndex, coordinateXIndex, coordinateZIndex);
        return tileCoordinate;
    }

}

// class to represent a coordinate in the Tile Coordinate System
public class TileCoordinate
{
    public int tileXIndex;
    public int tileZIndex;
    public int coordinateXIndex;
    public int coordinateZIndex;

    public TileCoordinate(int tileXIndex, int tileZIndex, int coordinateXIndex, int coordinateZIndex)
    {
        this.tileXIndex = tileXIndex;
        this.tileZIndex = tileZIndex;
        this.coordinateXIndex = coordinateXIndex;
        this.coordinateZIndex = coordinateZIndex;
    }
}


[System.Serializable]
public struct LODInfo
{
    [Range(0, MeshSettings.numSupportedLODs - 1)]
    public int lod;
    public float visibleDstThreshold;

    public float sqrVisibleDstThreshold
    {
        get
        {
            return visibleDstThreshold * visibleDstThreshold;
        }
    }
}