﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TreeGeneration : MonoBehaviour
{
    [SerializeField]
    private Object trackingSphere;

    //[SerializeField]
    //private NoiseGeneration noiseGeneration;

    //[SerializeField]
    //private Wave[] waves;

    //[SerializeField]
    //private float levelScale;

    //[SerializeField]
    //private float[] neighborRadius;

    [SerializeField]
    private int biomeIndex = 1;

    [SerializeField]
    private GameObject[] treePrefab;

    [SerializeField]
    private float waterHeight = 2f;

    [SerializeField]
    private float treeWater = 0.25f;

    [SerializeField]
    private float treeHeat = .2f;

    private int treeCount = 0;


    public void GenerateTrees(int tilesWidth, int verticesWidth, TerrainData terrainData)
    {
        // generate a tree noise map using Perlin Noise
        //float[,] treeMap = noiseGeneration.GeneratePerlinNoiseMap(levelWidth, levelDepth, this.levelScale, 0, 0, this.waves);
        {
            //tilesWidth * verticesWidth
            for (int xIndex = 0; xIndex < (tilesWidth * (verticesWidth - tilesWidth)); xIndex++)
            {
                for (int zIndex = 0; zIndex < (tilesWidth * (verticesWidth - tilesWidth)); zIndex++)
                    {
                        // convert from Level Coordinate System to Tile Coordinate System and retrieve the corresponding TileData
                        TileCoordinate tileCoordinate = terrainData.ConvertToTileCoordinate(xIndex, zIndex);
                        HeightMap treeHeightMap = terrainData.chunksData[tileCoordinate.tileXIndex, tileCoordinate.tileZIndex];

                    // get the terrain type of this coordinate
                    //TerrainType terrainType = tileData.chosenHeightTerrainTypes[tileCoordinate.coordinateXIndex, tileCoordinate.coordinateZIndex];

                    // get the biome of this coordinate

                    //Biome biome = tileData.chosenBiomes[tileCoordinate.coordinateXIndex, tileCoordinate.coordinateZIndex];

                    // check if it is a water terrain. Trees cannot be placed over the water
                    //if (treeHeightMap.heightvalues[tileCoordinate.coordinateXIndex, tileCoordinate.coordinateZIndex] >= waterHeight &&
                    //    treeHeightMap.heat[tileCoordinate.coordinateXIndex, tileCoordinate.coordinateZIndex] <= treeHeat &&
                    //    treeHeightMap.moisture[tileCoordinate.coordinateXIndex, tileCoordinate.coordinateZIndex] >= treeWater)
                    //{
                    //    //float treeValue = treeMap[xIndex, zIndex];

                    //    // compares the current tree noise value to the neighbor ones
                    //    //this is where you pick tree density
                    //    //int neighborZBegin = (int)Mathf.Max(0, zIndex - this.neighborRadius[biomeindex]);
                    //    //int neighborZEnd = (int)Mathf.Min(levelDepth - 1, zIndex + this.neighborRadius[biomeindex]);
                    //    //int neighborXBegin = (int)Mathf.Max(0, xIndex - this.neighborRadius[biomeindex]);
                    //    //int neighborXEnd = (int)Mathf.Min(levelWidth - 1, xIndex + this.neighborRadius[biomeindex]);
                    //    //float maxValue = 0f;
                    //    //for (int neighborZ = neighborZBegin; neighborZ <= neighborZEnd; neighborZ++)
                    //    //{
                    //    //    for (int neighborX = neighborXBegin; neighborX <= neighborXEnd; neighborX++)
                    //    //    {
                    //    //        float neighborValue = treeMap[neighborX, neighborZ];
                    //    //        // saves the maximum tree noise value in the radius
                    //    //        if (neighborValue >= maxValue)
                    //    //        {
                    //    //            maxValue = neighborValue;
                    //    //        }
                    //    //    }
                    //    //}

                    //    // if the current tree noise value is the maximum one, place a tree in this location
                    //    //if (treeValue == maxValue)
                    //    //{
                    //    // calculate the mesh vertex index
                    //    //Vector3[] meshVertices = tileData.mesh.vertices;
                    //    //int tileWidth = tileData.heightMap.GetLength(0);
                    //    //int vertexIndex = tileCoordinate.coordinateZIndex * tileWidth + tileCoordinate.coordinateXIndex;


                    //having an issue here.
                    //the trees start at bottom left, but somehow seem to be pulling height from bottom right of bottom left tile
                    //therefor what we need to do is to reassign the height values to pull from the bottom left.
                     if (treeHeightMap.heightvalues[(verticesWidth - 1 - tileCoordinate.coordinateXIndex), (verticesWidth - 1 - tileCoordinate.coordinateZIndex)] >= waterHeight&&
                        treeHeightMap.moisture[(verticesWidth - 1 - tileCoordinate.coordinateXIndex), (verticesWidth - 1 - tileCoordinate.coordinateZIndex)] >= treeWater&&
                        treeHeightMap.heat[(verticesWidth - 1 - tileCoordinate.coordinateXIndex), (verticesWidth - 1 - tileCoordinate.coordinateZIndex)] <= treeHeat
                        ) 
                     {
                        // if (treeHeightMap.moisture[(verticesWidth - 1 - tileCoordinate.coordinateXIndex), (verticesWidth - 1 - tileCoordinate.coordinateZIndex)] >= treeWater)

                        // minus tilesWidth is to adjust for overlapping vertices
                        Vector3 treePosition = new Vector3(xIndex - ((verticesWidth - tilesWidth) * tilesWidth / 2), treeHeightMap.heightvalues[(verticesWidth - 1 - tileCoordinate.coordinateXIndex), (verticesWidth - 1 - tileCoordinate.coordinateZIndex)],

                             //this is where the z goes... difficult.  each tile from top to bottom but tiles are from bottom up.  So we first add height for the tile (0,1,2) then subtrack it based on zindex)
                             //(tileCoordinate.tileZIndex+0.5f)*(verticesWidth-tilesWidth)
                             ((-zIndex - (verticesWidth - tilesWidth) / 2) + (2*(tileCoordinate.tileZIndex) * verticesWidth)));
                             GameObject tree = Instantiate(this.treePrefab[biomeIndex], treePosition, Quaternion.identity) as GameObject;

                            //this starts at 124 and goes down
                            //Debug.Log("Coordinate X Index: " + (verticesWidth - 1 - tileCoordinate.coordinateXIndex) + " CoordZIndex: " + (verticesWidth - 1 - tileCoordinate.coordinateZIndex));
                            //Debug.Log("X: " + xIndex + " Z: " + zIndex + " water: " + treeHeightMap.moisture[(verticesWidth - 1 - tileCoordinate.coordinateXIndex), (verticesWidth - 1 - tileCoordinate.coordinateZIndex)]);

                            //use this to make object bigger or smaller
                            //tree.transform.localScale = new Vector3(0.05f, 0.05f, 0.05f);
                            treeCount++;
                         zIndex++;
                      


                     }
                }
                        //}
                    //}
            }
        }
    }
}
