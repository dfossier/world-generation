﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RiverGeneration : MonoBehaviour
{
    [SerializeField]
    private int numberOfRivers;

    [SerializeField]
    private float heightThreshold;

    [SerializeField]
    private int looplimit = 200;

    [SerializeField]
    private float depressAmount = 5f;

    [SerializeField]
    private float riverPersistence = 1.5f;

    //[SerializeField]
    //private Color riverColor;

    [SerializeField]
    private float waterHeight = 2f;

    private bool hasneighbor = true;
    private bool haslakeneighbor = true;

    private HeightMap[,] riverchunksData;

    int levelWidth;
    int levelDepth;

    public void GenerateRivers(int levelDepth, int levelWidth, TerrainData terrainData)
    {
        this.levelDepth = levelDepth;
        this.levelWidth = levelWidth;
        for (int riverIndex = 0; riverIndex < numberOfRivers; riverIndex++)
        {
            // populate the data
            riverchunksData = new HeightMap[terrainData.levelDepthInTiles, terrainData.levelDepthInTiles];
            for (int i = 0; i < terrainData.levelDepthInTiles; i++)
            {
                for (int j = 0; j < terrainData.levelDepthInTiles; j++)
                {
                    riverchunksData[i, j] = terrainData.chunksData[i, j];
                }
            }
            // choose a origin for the river
            Vector3 riverOrigin = ChooseRiverOrigin(levelDepth, levelWidth, terrainData);
            // build the river starting from the origin
            BuildRiver(levelDepth, levelWidth, riverOrigin, terrainData);
        }
    }

    private Vector3 ChooseRiverOrigin(int levelDepth, int levelWidth, TerrainData terrainData)
    {
        bool found = false;
        int randomZIndex = 0;
        int randomXIndex = 0;
        // iterates until finding a good river origin
        while (!found)
        {
            // pick a random coordinate inside the level
            randomZIndex = Random.Range(0, levelDepth);
            randomXIndex = Random.Range(0, levelWidth);

            // convert from Level Coordinate System to Tile Coordinate System and retrieve the corresponding TileData
            TileCoordinate tileCoordinate = terrainData.ConvertToTileCoordinate(randomXIndex, randomZIndex);
            HeightMap heightMRiver = riverchunksData[tileCoordinate.tileXIndex, tileCoordinate.tileZIndex];

            // if the height value of this coordinate is higher than the threshold, choose it as the river origin
            float heightValue = heightMRiver.heightvalues[tileCoordinate.coordinateXIndex, tileCoordinate.coordinateZIndex];
            //print("River" + "X: " + randomXIndex + " Z: " + randomZIndex + " height value: " + heightValue);

            if (heightValue >= this.heightThreshold)
            {
                found = true;
            }
        }
        return new Vector3(randomXIndex, 0, randomZIndex);
    }


    private void BuildRiver(int levelDepth, int levelWidth, Vector3 riverOrigin, TerrainData terrainData)
    {
        int loopcount = 0;
        HashSet<Vector3> visitedCoordinates = new HashSet<Vector3>();
        // the first coordinate is the river origin
        Vector3 currentCoordinate = riverOrigin;
        bool foundWater = false;
        while (!foundWater)
        {
            // convert from Level Coordinate System to Tile Coordinate System and retrieve the corresponding TileData
            TileCoordinate tileCoordinate = terrainData.ConvertToTileCoordinate((int)currentCoordinate.x, (int)currentCoordinate.z);
            HeightMap heightMap = riverchunksData[tileCoordinate.tileXIndex, tileCoordinate.tileZIndex];
            //print("Use this coord" + tileCoordinate.coordinateXIndex + " for X and this for Z" + tileCoordinate.coordinateZIndex);

            // save the current coordinate as visited
            visitedCoordinates.Add(currentCoordinate);

            // check if we have found water
            if (heightMap.heightvalues[tileCoordinate.coordinateXIndex, tileCoordinate.coordinateZIndex] <= waterHeight)
            {
                //we stop
                //print("Found water, height = " + heightMap.heightvalues[tileCoordinate.coordinateXIndex, tileCoordinate.coordinateZIndex]);
                foundWater = true;
            }

            else

                //depress river
                terrainData.chunksData[tileCoordinate.tileXIndex, tileCoordinate.tileZIndex].heightvalues[tileCoordinate.coordinateXIndex, tileCoordinate.coordinateZIndex]
                    //    = -20;
                    = (heightMap.heightvalues[tileCoordinate.coordinateXIndex, tileCoordinate.coordinateZIndex]-depressAmount);
            
            // if we hit loop limit, we stop
            if (loopcount >= looplimit)
            {
                //Debug.Log("Loop limit reached: " + loopcount);
                foundWater = true;
            }
            else
            {
                
                // pick neighbor coordinates, if they exist
                List<Vector3> neighbors = new List<Vector3>();
                if (currentCoordinate.z > 0)
                {
                    neighbors.Add(new Vector3(currentCoordinate.x, 0, currentCoordinate.z - 1));
                }
                if (currentCoordinate.z < levelDepth - 1)
                {
                    neighbors.Add(new Vector3(currentCoordinate.x, 0, currentCoordinate.z + 1));
                }
                if (currentCoordinate.x > 0)
                {
                    neighbors.Add(new Vector3(currentCoordinate.x - 1, 0, currentCoordinate.z));
                }
                if (currentCoordinate.x < levelWidth - 1)
                {
                    neighbors.Add(new Vector3(currentCoordinate.x + 1, 0, currentCoordinate.z));
                }

                // find the minimum neighbor that has not been visited yet and flow to it
                float minHeight = float.MaxValue;
                Vector3 minNeighbor = new Vector3(0, 0, 0);
                foreach (Vector3 neighbor in neighbors)
                {
                    // convert from Level Coordinate System to Tile Coordinate System and retrieve the corresponding TileData
                    TileCoordinate neighborTileCoordinate = terrainData.ConvertToTileCoordinate((int)neighbor.x, (int)neighbor.z);
                    //print("Neighbor X: " + (int)neighbor.x + " Y: " + (int)neighbor.z + " Height: " + heightMap.heightvalues[neighborTileCoordinate.coordinateXIndex, neighborTileCoordinate.coordinateZIndex]);
                    HeightMap neighborHeightMap = terrainData.chunksData[neighborTileCoordinate.tileXIndex, neighborTileCoordinate.tileZIndex];
                    // if the neighbor is the lowest one and has not been visited yet, save it
                    float neighborHeight = heightMap.heightvalues[neighborTileCoordinate.coordinateXIndex, neighborTileCoordinate.coordinateZIndex];
                    if (neighborHeight < minHeight && !visitedCoordinates.Contains(neighbor))
                    {
                        hasneighbor = true;
                        minHeight = neighborHeight;
                        minNeighbor = neighbor;
                    }
                    if (hasneighbor == false)
                    {
                        //print("No neighbors");
                        //MakeALake(neighbor, terrainData, visitedCoordinates, foundWater);
                        //foundWater = true;
                    }
                }
                // flow to the lowest neighbor
                minHeight = + riverPersistence;
                currentCoordinate = minNeighbor;
                loopcount++;
                hasneighbor = false;
            }
        }
    }
}
