﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HeatMap = HeatMapGenerator.HeatMap;
using MoistureMap = MoistureMapGenerator.MoistureMap;

public static class HeightMapGenerator {

	public static HeightMap GenerateHeightMap (int width, int height, HeightMapSettings settings, Vector2 sampleCentre) {

		float[, ] heightvalues = Noise.GenerateNoiseMap (width, height, settings.noiseSettings, sampleCentre);
        AnimationCurve heightCurve_threadsafe = new AnimationCurve (settings.heightCurve.keys);

		float minHeight = float.MaxValue;
		float maxHeight = float.MinValue;

		for (int i = 0; i < width; i++) {
			for (int j = 0; j < height; j++) {
				heightvalues[i, j] *= heightCurve_threadsafe.Evaluate (heightvalues[i, j]) * settings.heightMultiplier;

				if (heightvalues[i, j] > maxHeight) {
					maxHeight = heightvalues[i, j];
				}
				if (heightvalues[i, j] < minHeight) {
					minHeight = heightvalues[i, j];
				}
			}
		}

        //TODO make the heatmap more realistic by passing heightmap to Heatmap and Moisturemap then adding heightmap*.5 to the heatmap
        MoistureMap moistureMap = MoistureMapGenerator.GenerateMoistureMap (width, height, settings, sampleCentre);
		HeatMap heatMap = HeatMapGenerator.GenerateHeatMap (width, height, settings, sampleCentre, heightvalues);



            return new HeightMap (
                heightvalues, minHeight, maxHeight,
			heatMap.heatvalues, heatMap.minHeat, heatMap.maxHeat,
			moistureMap.moisturevalues, moistureMap.minmoisture, moistureMap.maxmoisture);
			}

}

public struct HeightMap {
	public readonly float[, ] heightvalues;
	public readonly float minHeight;
	public readonly float maxHeight;

	public readonly float[, ] heat;
	public readonly float minHeat;
	public readonly float maxHeat;

	public readonly float[, ] moisture;
	public readonly float minMoisture;
	public readonly float maxMoisture;


	public HeightMap (float[, ] heightvalues, float minHeight, float maxHeight, float[, ] heat, float minHeat, float maxHeat, float[, ] moisture, float minMoisture, float maxMoisture) {
		this.heightvalues = heightvalues;
		this.minHeight = minHeight;
		this.maxHeight = maxHeight;

		this.heat = heat;
		this.minHeat = minHeat;
		this.maxHeat = maxHeat;

		this.moisture = moisture;
		this.minMoisture = minMoisture;
		this.maxMoisture = maxMoisture;



	}
}


